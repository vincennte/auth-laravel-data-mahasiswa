<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama_mahasiswa' => ['required','regex:/[a-z]/', 'min:3','max:30'],
            'nim' => ['required', 'numeric', 'min:2', 'digits:10'],
            'username' => ['required', 'max:255', 'unique:users'],
            'password' => ['required', 'min:8', 'confirmed','regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/'],
            'foto_mahasiswa' => ['required','mimes:jpeg,jpg,png|max:500'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $file_extention = $data['foto_mahasiswa']->getClientOriginalExtension();
        $file_name = time().'.'.$file_extention;
        $file_path = $data['foto_mahasiswa']->move(public_path('uploads'),$file_name);

        return User::create([
            'nama_mahasiswa' => $data['nama_mahasiswa'],
            'nim' => $data['nim'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            'foto_mahasiswa' =>  $file_name,
        ]);
    }
}
