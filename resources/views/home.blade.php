@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <center><div class="card-header">{{ __('Data Mahasiswa') }}</div></center>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col"><center>Nama Lengkap</center></th>
                            <th scope="col"><center>NIM</center></th>
                            <th scope="col"><center>Username</center></th>
                            <th scope="col"><center>Foto</center></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row"><center>{{Auth::User()->nama_mahasiswa}}</center></th>
                            <td><center>{{Auth::User()->nim}}</center></td>
                            <td><center>{{Auth::User()->username}}</center></td>
                            <p hidden>{{$foto = Auth::User()->foto_mahasiswa}} </p>
                            <td><center><img src="{{ asset("/uploads/$foto") }}" width="100px" height="150px"; /></center></td>                           
                          </tr>
                        </tbody>
                      </table>
                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
